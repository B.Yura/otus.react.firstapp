
import './App.css';
import LoginPage from './Identity/Login';
import HeaderMenu from './Components/HeaderMenu';
import { BrowserRouter as router, Route, Outlet, Link, BrowserRouter, Routes } from "react-router-dom";
import Home from './Home';

function App() {
  return (
    <div className="App" >
   
      <BrowserRouter>
      <HeaderMenu />
        <Routes>
          <Route index element={<Home />} />
          <Route path='login' element={<LoginPage />} />
        </Routes>
      </BrowserRouter>
    </div>

  );
}

export default App;
