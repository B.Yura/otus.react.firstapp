import React, { useState } from "react";

function SendLogin(login, password) {
    fetch("https://localhost:4000", {
        type: 'Post',
        data: {
            login,
            password
        }
    })
    window.alert('Выполнен вход');
}


function LoginPage() {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    return (
        <div className="container" >
            <form>
                <div className="row justify-content-center mb-3">
                    <label className="form-label">Email address</label>
                    <input type="email" className="form-control w-50" value={login} onChange={e => setLogin(e.target.value)} aria-describedby="emailHelp" />
                </div>
                <div className="row justify-content-center mb-3">
                    <label className="form-label">Password</label>
                    <input type="password" className="form-control w-50" value={password} onChange={e => setPassword(e.target.value)} />
                </div>
                <button type="submit" onClick={() => SendLogin(login, password)} className="btn btn-primary">Submit</button>
            </form>

        </div>
    )
}

export default LoginPage;