import React from "react";
import { BrowserRouter as router, Route, Outlet, Link, BrowserRouter, Routes } from "react-router-dom";
import LoginPage from "../Identity/Login";

function HeaderMenu() {
    return (

        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container">
                <a className="navbar-brand" href="/">Navbar</a>

                <ul className="navbar-nav">
                    <li className="nav-item">
                        <a className="nav-link active" aria-current="page" href="/login">Войти</a>
                    </li>
                </ul>
            </div>
        </nav>

    )
}

export default HeaderMenu;